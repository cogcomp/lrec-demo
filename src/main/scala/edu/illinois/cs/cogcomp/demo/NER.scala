package edu.illinois.cs.cogcomp.demo

import edu.illinois.cs.cogcomp.core.datastructures.ViewNames
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.Constituent
import edu.illinois.cs.cogcomp.edison.features.factory.BrownClusterFeatureExtractor
import edu.illinois.cs.cogcomp.edison.features.lrec.POSWindow
import edu.illinois.cs.cogcomp.saul.classifier.{Learnable, SparseNetworkLBP}
import edu.illinois.cs.cogcomp.saul.datamodel.DataModel

import scala.collection.JavaConversions._

object NERDataModel extends DataModel {
  val word = node[Constituent]

  val NERLabel = property(word) {
    x: Constituent => x.getLabel
  }
  val surface = property(word) {
    x: Constituent => x.getSurfaceForm
  }
  val pos = property(word) {
    x: Constituent => new POSWindow(ViewNames.POS).getFeatures(x).toString
  }
  val brown = property(word) {
    x: Constituent => BrownClusterFeatureExtractor.instance320.getFeatures(x).toString
  }
}

import edu.illinois.cs.cogcomp.demo.NERDataModel._
object NERClassifier extends Learnable[Constituent](word) {
  override def feature = using(surface, pos)

  def label = NERLabel
  override lazy val classifier = new SparseNetworkLBP
}

object NERApp {
  val dataDir = "src/main/resources"

  def main(args: Array[String]) {
    val trainReader = new NERDataReader(dataDir + "/train.gz", ViewNames.NER_CONLL)
    val trainWords = trainReader.getTextAnnotations.flatMap(x => trainReader.candidateGenerator(x))
    word.populate(trainWords)
    NERClassifier.learn(10)

    val testReader = new NERDataReader(dataDir + "/test.gz", ViewNames.NER_CONLL)
    val testWords = testReader.getTextAnnotations.flatMap(x => testReader.candidateGenerator(x))
    word.populate(testWords, train = false)
    NERClassifier.test()
  }
}
