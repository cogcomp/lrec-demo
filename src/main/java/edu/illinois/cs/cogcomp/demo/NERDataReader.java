package edu.illinois.cs.cogcomp.demo;

import edu.illinois.cs.cogcomp.annotation.AnnotatorException;
import edu.illinois.cs.cogcomp.annotation.AnnotatorService;
import edu.illinois.cs.cogcomp.annotation.BasicTextAnnotationBuilder;
import edu.illinois.cs.cogcomp.core.datastructures.ViewNames;
import edu.illinois.cs.cogcomp.core.datastructures.textannotation.*;
import edu.illinois.cs.cogcomp.core.io.IOUtils;
import edu.illinois.cs.cogcomp.core.io.LineIO;
import edu.illinois.cs.cogcomp.core.utilities.configuration.Configurator;
import edu.illinois.cs.cogcomp.lbjava.parse.Parser;
import edu.illinois.cs.cogcomp.nlp.common.PipelineConfigurator;
import edu.illinois.cs.cogcomp.nlp.pipeline.IllinoisPipelineFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class NERDataReader implements Parser {
    private static String[] viewsToAdd = {ViewNames.POS};

    private static Logger logger = LoggerFactory.getLogger(NERDataReader.class);

    private static final String CANDIDATE = "candidate";

    private List <TextAnnotation> textAnnotations;
    private List<Constituent> candidates;
    private int currentCandidate, currentTextAnnotation;
    private String viewName;
    private final String data;

    private AnnotatorService preprocessor;

    public NERDataReader(String file, String viewName) {
        this.data = file;
        this.viewName = viewName;
        this.candidates = new ArrayList<>();

        List<TextAnnotation> rawTAs = readData();
        int processed = 0;
        int total = rawTAs.size();
        textAnnotations = new ArrayList<>();
        logger.info("Finished reading from {}.", this.data);
        for (TextAnnotation ta : rawTAs) {
            try {
                for (String view : viewsToAdd) getPreprocessor().addView(ta, view);
                textAnnotations.add(ta);
            } catch (AnnotatorException | RuntimeException | IOException e) {
                logger.error("Unable to preprocess TextAnnotation {}. Skipping", ta.getId());
                continue;
            }
            processed++;
            if (processed % 1000 == 0)
                logger.info("Processed {} of {} TextAnnotations", processed, total);
        }
        logger.info("Finished pre-processing {} TextAnnotations.", processed);
    }

    private List<TextAnnotation> readData() {
        List<TextAnnotation> textAnnotations = new ArrayList<>();
        List<String> lines;
        try {
            lines = LineIO.readGZip(data);
        } catch (java.io.IOException e) {
            throw new RuntimeException("Couldn't read " + data);
        }
        String corpusId = IOUtils.getFileName(data);
        List<String> labels = new ArrayList<>();
        List<String> tokens = new ArrayList<>();
        int taId = 0;
        for (String line : lines) {
            if (line.isEmpty()) {
                List<String[]> tokenizedSentence = Collections.singletonList(tokens.toArray(new String[tokens.size()]));
                TextAnnotation ta = BasicTextAnnotationBuilder.createTextAnnotationFromTokens(
                        corpusId, String.valueOf(taId), tokenizedSentence);

                if (isAllPunct(tokens)) {
                    logger.info("Skipping empty sentence {} ("+corpusId+":sent-{}).", ta.getText().trim(), ta.getId());
                    continue;
                }
                addView(ta, labels);
                textAnnotations.add(ta);
                labels.clear();
                tokens.clear();
                taId++;
            }
            else {
                labels.add(line.split("\\s+")[0]);
                tokens.add(line.split("\\s+")[5]);
            }
        }

        return textAnnotations;
    }

    private AnnotatorService getPreprocessor() throws IOException, AnnotatorException {
        if (preprocessor == null) {
            Map<String, String> nonDefaultProps = new HashMap<>();
            nonDefaultProps.put(PipelineConfigurator.USE_POS.key, Configurator.TRUE);
            nonDefaultProps.put(PipelineConfigurator.USE_LEMMA.key, Configurator.FALSE);
            nonDefaultProps.put(PipelineConfigurator.USE_NER_CONLL.key, Configurator.FALSE);
            nonDefaultProps.put(PipelineConfigurator.USE_NER_ONTONOTES.key, Configurator.FALSE);
            nonDefaultProps.put(PipelineConfigurator.USE_SHALLOW_PARSE.key, Configurator.FALSE);
            nonDefaultProps.put(PipelineConfigurator.USE_STANFORD_DEP.key, Configurator.FALSE);
            nonDefaultProps.put(PipelineConfigurator.USE_STANFORD_PARSE.key, Configurator.FALSE);
            nonDefaultProps.put(PipelineConfigurator.USE_SRL_VERB.key, Configurator.FALSE);
            nonDefaultProps.put(PipelineConfigurator.USE_SRL_NOM.key, Configurator.FALSE);
            preprocessor = IllinoisPipelineFactory.buildPipeline(new PipelineConfigurator().getConfig(nonDefaultProps));
        }
        return preprocessor;
    }

    private boolean isAllPunct(List<String> tokens) {
        boolean allPunct = true;
        for (String token : tokens){
            allPunct &= token.matches("\\p{Punct}");
        }
        return allPunct;
    }

    private void addView(TextAnnotation ta, List<String> labels) {
        TokenLabelView labelView = new TokenLabelView(viewName, ta);
        List constituents = ta.getView(ViewNames.TOKENS).getConstituents();

        assert constituents.size() == labels.size();

        for (int i = 0; i < constituents.size(); ++i) {
            Constituent constituent = (Constituent) constituents.get(i);
            labelView.addTokenLabel(constituent.getStartSpan(), labels.get(i), 1.0D);
        }
        ta.addView(viewName, labelView);
    }

    public List<Constituent> candidateGenerator(TextAnnotation ta) {
        return getFinalCandidates(ta.getView(viewName), ta.getView(ViewNames.TOKENS).getConstituents());
    }

    private List<Constituent> getFinalCandidates(View goldView, List<Constituent> candidates) {
        List<Constituent> finalCandidates = new ArrayList<>();
        for (Constituent c : candidates) {
            Constituent goldConst = getExactMatch(goldView, c);
            if (goldConst != null)
                finalCandidates.add(goldConst);
            else
                finalCandidates.add(new Constituent(CANDIDATE, viewName, c.getTextAnnotation(), c.getStartSpan(), c.getEndSpan()));
        }
        for (Constituent c : goldView.getConstituents()) {
            if (!finalCandidates.contains(c))
                finalCandidates.add(c);
        }
        return finalCandidates;
    }

    private Constituent getExactMatch(View view, Constituent c) {
        for (Constituent viewConst : view.getConstituents()) {
            if (viewConst.getSpan().equals(c.getSpan())) return viewConst;
        }
        return null;
    }

    public List<TextAnnotation> getTextAnnotations() {
        return textAnnotations;
    }

    /**
     * Fetches the next available data instance for training/testing. Also, pre-processes each new
     * {@link TextAnnotation} object before accessing its members.
     *
     * @return A {@link Constituent} (which might be a part of a {@link Relation},
     *         depending on the type of {@link View} )
     */
    @Override
    public Object next() {
        if (candidates.isEmpty() || candidates.size() == currentCandidate) {
            if (textAnnotations.size() <= currentTextAnnotation) return null;
            TextAnnotation ta = textAnnotations.get(currentTextAnnotation);
            if (!ta.hasView(viewName)) return next();
            candidates = candidateGenerator(ta);
            if (candidates.isEmpty()) return next();
            currentCandidate = 0;
            currentTextAnnotation++;
            if (currentTextAnnotation % 1000 == 0)
                logger.info("Read {} TextAnnotations", currentTextAnnotation);
        }
        return candidates.get(currentCandidate++);
    }

    @Override
    public void reset() {
        currentCandidate = 0;
        candidates = new ArrayList<>();
        currentTextAnnotation = 0;
    }

    @Override
    public void close() {}
}
