name := "lrec-demo"

version := "1.0"

scalaVersion := "2.11.8"
val illinoisCogCompVersion = "3.0.40"

resolvers += "CogComp" at "http://cogcomp.cs.illinois.edu/m2repo"

libraryDependencies ++= Seq(
  "edu.illinois.cs.cogcomp" % "LBJava" % "1.2.16",
  "edu.illinois.cs.cogcomp" % "illinois-core-utilities" % illinoisCogCompVersion,
  "edu.illinois.cs.cogcomp" % "illinois-edison" % illinoisCogCompVersion,
  "edu.illinois.cs.cogcomp" % "illinois-nlp-pipeline" % "0.1.21",
  "edu.illinois.cs.cogcomp" %% "saul" % "0.2"
)